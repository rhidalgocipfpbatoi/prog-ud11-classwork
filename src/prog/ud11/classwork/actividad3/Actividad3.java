/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud11.classwork.actividad3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author batoi
 */
public class Actividad3 {
      public static void main(String[] args) {
        escribirFichero("\nProgramación DAM/DAW");
    }
    private static void escribirFichero(String cadena) {
        File file = new File("resources/actividad3/fichero.txt");
        try {
            FileWriter fileWriter = new FileWriter(file, true);
            fileWriter.write(cadena);
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
