/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud11.classwork.actividad5;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author batoi
 */
public class Actividad5 {
     public static void main(String[] args) {
        ArrayList<String> modulos = leerFichero();
        escribirFichero(modulos);
    }

    private static ArrayList<String> leerFichero() {
        try {
            ArrayList<String> modulos = new ArrayList<>();
            File file = new File("resources/actividad5/modulos.txt");
            FileReader fileReader = new FileReader(file);
            BufferedReader buffer = new BufferedReader(fileReader);
            do{
                String line = buffer.readLine();
                if (line == null) {
                    buffer.close();
                    return modulos;
                }
                modulos.add(line);
            }while (true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static void escribirFichero(ArrayList<String> modulos) {
        File file = new File("resources/actividad5/modulosMayuscula.txt");
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file));
            for (String modulo : modulos) {
                String moduloEnMayuscula = modulo.toUpperCase();
                bufferedWriter.write(moduloEnMayuscula);
                bufferedWriter.newLine();
            }
            bufferedWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
