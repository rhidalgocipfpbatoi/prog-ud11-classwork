/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud11.classwork.actividad4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author batoi
 */
public class Actividad4 {
    
    public static void main(String[] args) {
        leerFichero();
    }

    private static void leerFichero() {
        try {
            int totalProductos = 0;
            File file = new File( "resources/actividad4/productos.txt");
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            do{
                String line = bufferedReader.readLine();
                if (line == null) {
                    System.out.println("El total de productos es " + totalProductos);
                    bufferedReader.close();
                    return;
                }
                String[] elementos = line.split(",");

                String producto = elementos[1].trim();
                int numProductos = Integer.parseInt(elementos[2].trim());

                if (producto.equals("Televisión")
                        || producto.equals("Monitor")
                        || producto.equals("Teclado"))
                    totalProductos += numProductos;
            }while (true);
        } catch (FileNotFoundException ex) {
            System.out.println("El archivo no existe" + ex.getMessage());
        }
        catch (IOException e) {
            System.out.println("Se ha producido durante la lectura del archivo" + e.getMessage());
        }
    }

}
