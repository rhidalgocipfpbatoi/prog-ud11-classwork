/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud11.classwork.actividad2;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author batoi
 */
public class Actividad2 {

    public static void main(String[] args) {
        System.out.println(leerContenidoFichero());
    }
    private static String leerContenidoFichero() {
        StringBuilder contenidoFichero = new StringBuilder();
        try {
            FileReader fileReader = new FileReader("resources/actividad2/fichero.txt");
            int character;
            do {
                character = fileReader.read();
                if (character >= 0) {
                    contenidoFichero.append((char) character);
                } else {
                    fileReader.close();
                }
            }while (character >= 0);
        } catch (FileNotFoundException e) {
            System.out.println("El fichero que quieres leer no existe");
        } catch (IOException e) {
            System.out.println("Se ha producido un error en el acceso al fichero");
        }
        return  contenidoFichero.toString();
    }

}

