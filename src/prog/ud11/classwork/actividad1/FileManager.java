/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud11.classwork.actividad1;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author batoi
 */
public class FileManager {
    
    public static void crearFichero(String rutaDirectorio, String nombreFichero) {
        
        File directorio = new File(rutaDirectorio);
        if (!directorio.exists()){
            directorio.mkdir();
        }
        
        File fichero = new File(rutaDirectorio, nombreFichero);
        try {
            if (!fichero.exists()) {
                fichero.createNewFile();
            }
        } catch (IOException ex) {
            System.out.println("El fichero no se ha podido crear");
        }
    }
    
    public static void verArchivosDirectorio(String rutaDirectorio) {
        File directorio = new File(rutaDirectorio);
        
        if (!directorio.exists()) {
            System.out.println("El directorio no existe");
        } else {
            String[] nombresElementos = directorio.list();
            
            for (String nombre : nombresElementos) {
                System.out.println(nombre);
            }
        }
    }
    
    public static void verInformacionArchivo(String rutaDirectorio, String nombreArchivo) {
        File directorio = new File(rutaDirectorio);
        
        if (!directorio.exists()) {
            System.out.println("El directorio no existe");
        } else {
            File fichero = new File(rutaDirectorio, nombreArchivo);
            
            if (!fichero.exists()) {
                System.out.println("El fichero no existe");
            } else {
                System.out.println(fichero.getName());
                System.out.println(fichero.getAbsolutePath());
                System.out.println(fichero.getPath());
                System.out.println(fichero.canRead());
                System.out.println(fichero.canWrite());
                System.out.println(fichero.isDirectory());
                System.out.println(fichero.isFile());
            }
            
        }
    }
    
}
