/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud11.classwork.actividad1;

/**
 *
 * @author batoi
 */
public class Actividad1 {
    public static void main(String[] args) {
        FileManager.crearFichero("resources/actividad1", "hola.txt");
        FileManager.verArchivosDirectorio("resources/actividad2");
        FileManager.verInformacionArchivo("resources/actividad1", "");
    }
}
