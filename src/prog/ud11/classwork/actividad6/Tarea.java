/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud11.classwork.actividad6;

/**
 *
 * @author batoi
 */
public class Tarea {
    private int id;
    private String descripcion;
    private String usuario;
    private boolean realizada;

    public Tarea(int id, String descripcion, String usuario, boolean realizada) {
        this.id = id;
        this.descripcion = descripcion;
        this.usuario = usuario;
        this.realizada = realizada;
    }

    public int getId() {
        return id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getUsuario() {
        return usuario;
    }

    public String isRealizada() {
        return realizada?"SI":"NO";
    }

    @Override
    public String toString() {
        return "Tarea{" + "id=" + id + ", descripcion=" + descripcion + ", usuario=" + usuario + ", realizada=" + realizada + '}';
    }
 
    
    
}
