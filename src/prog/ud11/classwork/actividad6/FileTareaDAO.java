/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud11.classwork.actividad6;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
/**
 *
 * @author batoi
 */
public class FileTareaDAO {
    
    public Tarea findByCod(int cod) throws NotFoundException {
        
        BufferedReader bufferedReader;
        
        try {
            File file = new File("resources/actividad6/tareas.txt");
            FileReader fileReader = new FileReader(file);
            bufferedReader = new BufferedReader(fileReader);
            
            do {
                
                String linea = bufferedReader.readLine();
                
                if (linea == null) {
                    bufferedReader.close();
                    throw new NotFoundException();
                }
                
                String[]datosTarea = linea.split(";");
                if (datosTarea[0].equals(String.valueOf(cod))) {
                    String descripcion = datosTarea[1];
                    String usuario = datosTarea[2];
                    boolean realizada = datosTarea[3].equals("SI");
                    bufferedReader.close();
                    return new Tarea(cod, descripcion, usuario, realizada);
                }
                
            } while(true);
            
        } catch(FileNotFoundException ex) {
            System.out.println("Error al leer el fichero. " + ex.getMessage());
        } catch(IOException ex) {
            System.out.println("Error de entrada salida al leer." + ex.getMessage());
        } 
        
        return null;
        
    }
    
    public ArrayList<Tarea> findAll() {
        
        ArrayList<Tarea> tareas = new ArrayList<>();
                
        BufferedReader bufferedReader;
        
        try {
            File file = new File("resources/actividad6/tareas.txt");
            FileReader fileReader = new FileReader(file);
            bufferedReader = new BufferedReader(fileReader);
            
            do {
                
                String linea = bufferedReader.readLine();
                
                if (linea == null) {
                    bufferedReader.close();
                    return tareas;
                }
                
                String[]datosTarea = linea.split(";");
                
                int cod = Integer.parseInt(datosTarea[0]);
                String descripcion = datosTarea[1];
                String usuario = datosTarea[2];
                boolean realizada = datosTarea[3].equals("SI");
                Tarea tarea = new Tarea(cod, descripcion, usuario, realizada);
                tareas.add(tarea);
                
            } while(true);
            
        } catch(FileNotFoundException ex) {
            System.out.println("Error al leer el fichero. " + ex.getMessage());
        } catch(IOException ex) {
            System.out.println("Error de entrada salida al leer." + ex.getMessage());
        } 
        
        return null;
    }
    
    public ArrayList<Tarea> findAll(String usuario) {
        ArrayList<Tarea> tareas = new ArrayList<>();
                
        BufferedReader bufferedReader;
        
        try {
            File file = new File("resources/actividad6/tareas.txt");
            FileReader fileReader = new FileReader(file);
            bufferedReader = new BufferedReader(fileReader);
            
            do {
                
                String linea = bufferedReader.readLine();
                
                if (linea == null) {
                    bufferedReader.close();
                    return tareas;
                }
                
                String[]datosTarea = linea.split(";");

                if (datosTarea[2].equals(usuario)) {
                    int cod = Integer.parseInt(datosTarea[0]);
                    String descripcion = datosTarea[1];
                    boolean realizada = datosTarea[3].equals("SI");
                    Tarea tarea = new Tarea(cod, descripcion, usuario, realizada);
                    tareas.add(tarea);
                }
                
            } while(true);
            
        } catch(FileNotFoundException ex) {
            System.out.println("Error al leer el fichero. " + ex.getMessage());
        } catch(IOException ex) {
            System.out.println("Error de entrada salida al leer." + ex.getMessage());
        } 
        
        return null;
    }
    
    public void add(Tarea tarea) {
        try {
            File file = new File("resources/actividad6/tareas.txt");
            FileWriter fileWriter = new FileWriter(file, true);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            
            String linea = String.format("%d;%s;%s;%s", tarea.getId(), 
                    tarea.getDescripcion(), tarea.getUsuario(), tarea.isRealizada());
            bufferedWriter.append(linea);
            bufferedWriter.newLine();
            bufferedWriter.close();
            
        } catch (IOException ex) {
            System.out.println("Error en la apertura del fichero" + ex.getMessage());
        }
    }
}
