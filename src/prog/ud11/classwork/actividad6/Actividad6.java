/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud11.classwork.actividad6;

import java.util.Arrays;
import java.util.Collections;

/**
 *
 * @author batoi
 */
public class Actividad6 {
    
    public static void main(String[] args) {
        FileTareaDAO fileTareaDAO = new FileTareaDAO();
        System.out.println(Arrays.toString(fileTareaDAO.findAll().toArray()));    
        System.out.println("");
        System.out.println("");
        System.out.println(Arrays.toString(fileTareaDAO.findAll("Roberto").toArray()));
        System.out.println("");
        System.out.println("");
        try {
        System.out.println(fileTareaDAO.findByCod(2));
        } catch (NotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        System.out.println("");
        System.out.println("");
        Tarea tarea = new Tarea(4, "Sacar al perro", "Pepe", false);
        fileTareaDAO.add(tarea);
        System.out.println(Arrays.toString(fileTareaDAO.findAll().toArray()));    
        
    }
}
