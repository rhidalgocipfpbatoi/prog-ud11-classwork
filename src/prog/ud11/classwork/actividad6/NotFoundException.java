/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package prog.ud11.classwork.actividad6;

/**
 *
 * @author batoi
 */
public class NotFoundException extends Exception {
    
    public NotFoundException() {
        super("La tarea no existe");
    }
}
